(function(){
	if (window.recruitr) return;
	window.recruitr = {};

	window.recruitr.getSource = function getSource() {
		return document.body.innerHTML;
	};

	window.recruitr.getRuler = function getRuler() {
		var ruler = prompt("Enter the last ruler messaged, or leave blank to send messages to everyone on this page:", "");
		return ruler;
	};

	window.recruitr.compose = function compose(rulerInfos, message) {
		// Add the rulers to the "carboncopy" field, except for the first
		document.getElementById("carboncopy").textContent = rulerInfos.slice(1).map(function(rulerInfo) { return rulerInfo.ruler }).join("\n");
		document.getElementById("count").textContent = rulerInfos.length - 1;

		// Auto-fill the message fields
		if (message) {
			document.getElementsByName("messaging_subject")[0].value = message.subject;
			document.getElementById("messaging_message").value = message.message;
		}

		// Write forum string
		var forumString = "[b]Last Nation Messaged:[/b] http://www.cybernations.net/nation_drill_display.asp?Nation_ID="+rulerInfos[0].id+"\n"
			+ "[b]Ruler:[/b] "+rulerInfos[0].ruler+"\n"
			+ "[b]Nation Name:[/b] "+rulerInfos[0].name+"\n"
			+ "[b]Messages:[/b] "+rulerInfos.length+"\n"
			+ "\n"
			+ rulerInfos.map(function(rulerInfo){ return rulerInfo.ruler }).join("\n");
		
		var target = document.getElementsByName("submit2")[0].parentNode.parentNode.parentNode.firstChild;
		var elem1 = document.createElement("p");
		elem1.textContent = "Press Ctrl/Cmd+C to Copy, and Paste on Forum:";
		var elem2 = document.createElement("textarea");
		elem2.setAttribute("style", "width: 80%; height: 2em;");
		elem2.textContent = forumString;
		target.appendChild(elem1);
		target.appendChild(elem2);
		elem2.select();
		elem2.focus();
	};

})();

