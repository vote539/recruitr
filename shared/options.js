// from http://stackoverflow.com/a/105074
function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
}

function Message() {
	var self = this;

	self.id = ko.observable(guid());
	self.subject = ko.observable("Join My Alliance");
	self.message = ko.observable("CAUTION! Delete this placeholder and put your message here.");

	self.displayName = ko.computed(function() {
		return self.subject();
	});
	self.subjectChars = ko.computed(function() {
		return self.subject().length;
	});
	self.messageChars = ko.computed(function() {
		return self.message().length;
	});
}

var viewModel = {};

viewModel.messages = ko.observableArray();
viewModel.defaultMessage = ko.observable();
viewModel.followUpMessage = ko.observable();
viewModel.currentMessage = ko.observable();  // editor window
viewModel.numRulers = ko.observable(0);
viewModel.passcode = ko.observable();

viewModel.createNewMessage = function createNewMessage() {
	var message = new Message();
	viewModel.messages.push(message);
	viewModel.currentMessage(message);
};

viewModel.clearRulers = function clearRulers() {
	window.clearRulers();
};

window.rulersCleared = function rulersCleared() {
	viewModel.numRulers(0);
};

// FUNCTION TO SAVE KNOCKOUT MODEL AS JSON
viewModel.save = function save() {
	var prefs = {};

	// Save Messages Array
	prefs.messages = ko.toJS(viewModel.messages);

	// Save Default Messages
	if (viewModel.defaultMessage())
		prefs.defaultMessage = ko.toJS(viewModel.defaultMessage);
	if (viewModel.followUpMessage())
		prefs.followUpMessage = ko.toJS(viewModel.followUpMessage);

	// Save Passcode
	prefs.passcode = viewModel.passcode();

	// Send to backend
	window.savePrefs(prefs);
};

// FUNCTION TO READ JSON INTO KNOCKOUT MODEL
window.setPrefs = function setPrefs(prefs, numRulers) {
	if (!prefs) return;

	// Set Messages Array
	viewModel.messages.removeAll();
	viewModel.currentMessage(null);
	prefs.messages.forEach(function(_message) {
		var message = new Message();
		message.id(_message.id);
		message.subject(_message.subject);
		message.message(_message.message);
		viewModel.messages.push(message);
	});

	// Set Default Messages
	if (prefs.defaultMessage) {
		viewModel.defaultMessage(viewModel.messages().filter(function(message) {
			return (message.id() === prefs.defaultMessage.id);
		})[0]);
	}
	if (prefs.followUpMessage) {
		viewModel.followUpMessage(viewModel.messages().filter(function(message) {
			return (message.id() === prefs.followUpMessage.id);
		})[0]);
	}

	// Set Passcode and numRulers
	viewModel.passcode(prefs.passcode);
	viewModel.numRulers(numRulers);
};

ko.applyBindings(viewModel);
