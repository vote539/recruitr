var sha256;
if (typeof require !== "undefined") sha256 = require("./sha256.js");
else sha256 = window.sha256;

function RecruitrHandler() {
	var self = this;

	self.danSource = null;
	self.outboxSource = null;

	var SALT = "a4WAqMRy7BwQEqdH";
	var HASH = "68671ab0c095683dc84676f0662ff1f0c8f30ed36867f532ac69253040c7f641";
	self.testPasscode = function(passcode) {
		return HASH === sha256(SALT+passcode);
	};

	self.onSource = function(source) {
		if (/All Nations Display/.test(source)) {
			self.danSource = source;
			self.doGetRuler();
		} else if (/Sent Messages/.test(source)) {
			self.outboxSource = source;
			self.doOutbox();
		} else if (/Compose/.test(source)) {
			self.onCompose();
		} else {
			self.doAlert("I don't know how to parse this Cyber Nations page.");
		}
	}

	self.danSourceToRulerList = function() {
		// Matches all nations.
		var re = /Display Nation.*?>([^<]+)<[\S\s]*?<a href="send_message\.asp\?Nation_ID=(\d+)"><img.*?title\="Ruler: ([^"]+)".*?><\/a>\s+?(.*?Alliance:.*?|)\s+?(?:<\/p>)?<\/td>/g;
		var rulerInfos = [];
		var match;
		while ((match=re.exec(self.danSource)) !== null) {
			rulerInfos.push({
				name: match[1],
				id: match[2],
				ruler: match[3],
				hasAA: !!match[4]
			});
		}
		return rulerInfos;
	}

	self.onRuler = function(lastRulerMessaged) {
		if (!lastRulerMessaged && lastRulerMessaged !== "") return;  // Cancel button

		var rulerInfos = self.danSourceToRulerList();
		self.rulerInfosToMessage = [];
		var found = false;
		rulerInfos.forEach(function(rulerInfo) {
			if (found) return;
			if (rulerInfo.ruler.toLowerCase() === lastRulerMessaged.toLowerCase()) {
				found = true;
			} else if (!rulerInfo.hasAA) {
				// ruler is not in alliance
				self.rulerInfosToMessage.push(rulerInfo);
			}
		});

		if (self.rulerInfosToMessage.length === 0) {
			self.doAlert("No new rulers were found on this page.");
			self.doDestroy();
			return;
		}

		if (!found && lastRulerMessaged) {
			self.doAlert("The last ruler was not found on this page.  After you send these messages, you should go to the next page and send more messages until you find the last ruler messaged.");
		}

		// var composeUrl = "file:///Users/sffc/Documents/Projects/recruitr/resources/cybernations.net_sample_COMPOSE.html";
		var composeUrl = "http://www.cybernations.net/send_message.asp?Nation_ID=" + self.rulerInfosToMessage[0].id;

		self.doGoToCompose(composeUrl);
	}

	self.outboxSourceToMessagesList = function() {
		// Matches all messages.
		var re = /(message_opened|message_unopened)\.png[\s\S]+?nation_drill_display\.asp\?Nation_ID=(\d+)">([^<]+)<\/a>[\s\S]+?message_sent\.asp\?ID=\d+">([^<]+)</g;
		var rulerInfos = [];
		var match;
		while ((match=re.exec(self.outboxSource)) !== null) {
			rulerInfos.push({
				opened: match[1] === "message_opened",
				id: match[2],
				ruler: match[3],
				subject: match[4]
			});
		}
		return rulerInfos;
	}

	self.onOutbox = function(followedUpRulers, recruitmentMessageSubjects) {
		var rulerInfos = self.outboxSourceToMessagesList();
		self.rulerInfosToMessage = [];
		rulerInfos.forEach(function(rulerInfo) {
			if (rulerInfo.opened && followedUpRulers.indexOf(rulerInfo.ruler) === -1 && recruitmentMessageSubjects.indexOf(rulerInfo.subject) !== -1) {
				self.rulerInfosToMessage.push(rulerInfo);
			}
		});

		if (self.rulerInfosToMessage.length === 0) {
			self.doAlert("No follow-up messages to send from this page.");
			self.doDestroy();
			return;
		}

		// var composeUrl = "file:///Users/sffc/Documents/Projects/recruitr/resources/cybernations.net_sample_COMPOSE.html";
		var composeUrl = "http://www.cybernations.net/send_message.asp?Nation_ID=" + self.rulerInfosToMessage[0].id;

		self.doAddFollowedUpRulers(self.rulerInfosToMessage.map(function(rulerInfo){ return rulerInfo.ruler }));
		self.doGoToCompose(composeUrl);
	}

	self.onCompose = function() {
		self.doCompose(self.rulerInfosToMessage, (!!self.danSource) ? "default" : "follow-up");
		self.doDestroy();
	}
}

if (typeof module !== "undefined") module.exports = RecruitrHandler;
else window.RecruitrHandler = RecruitrHandler;
