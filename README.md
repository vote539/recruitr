Recruitr
========

This is the source code for sffc1's browser extension for sending recruitment mesages in Cyber Nations.

All code is licensed under the GNU General Public License version 3.

## Developing for Firefox Quantum

Install the web-ext tool from npm:

	$ npm install -g web-ext

To run the extension, I use the following command line:

	$ (rm -rf tmp && cp -r recruitr-quantum tmp && cd tmp && web-ext run)

web-ext run does not like symlinks, which is why you need to copy the extension to a temporary directory (to resolve symlinks).

To upload to the store:

1. Create a zip file:

	$ (cd recruitr-quantum && zip -r ../builds/quantum-VERSION.zip *)

2. Upload the zip file https://addons.mozilla.org/en-US/developers/addon/recruitr-for-firefox/edit (click "Upload New Version")


## Developing for Firefox Legacy (no longer supported)

*Prerequisite:* Install the `jpm` utility according to [these instructions](https://developer.mozilla.org/en-US/Add-ons/SDK/Tools/jpm).

Once you have `jpm` installed, you can run the extension by opening a terminal window to the *recruitr-firefox* directory and executing `jpm run`.

## Developing for Chrome

Open the Chrome extensions panel, and click the checkbox to enable "Developer Mode".  From there, click the "Load Unpacked Extension" button and select the *recruitr-chrome* directory.

To upload to the store:

1. Create a zip file:

	$ (cd recruitr-chrome && zip -r ../builds/chrome-VERSION.zip *)

2. Upload it at https://chrome.google.com/webstore/developer/edit/cmnakmlchjgcaohkoombcichkpfoeknh

3. Click Publish and make sure that update is published and not just a "draft"
