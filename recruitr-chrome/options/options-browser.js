window.savePrefs = function(prefs) {
	chrome.storage.sync.set({ "prefs": prefs }, function(){
		// the alert box doesn't show
		alert("Your settings have been saved.");
	});
};

window.clearRulers = function(next) {
	chrome.storage.sync.set({ "followedUpRulers": [] }, function(){
		window.rulersCleared();
	});
};

chrome.storage.sync.get(["prefs", "followedUpRulers"], function(results){
	if (!results.prefs) return window.setPrefs(null);
	var rulers = results.followedUpRulers;
	if (!rulers) rulers = [];
	window.setPrefs(results.prefs, rulers.length);
});
