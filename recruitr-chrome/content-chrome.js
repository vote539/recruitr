if (!window.recruitr.callbacks) {
	window.recruitr.callbacks = true;

	chrome.runtime.onMessage.addListener(function(message) {
		if (message.name === "get-source") {
			var response = window.recruitr.getSource();
			chrome.runtime.sendMessage({ name: "source", response: response });

		} else if (message.name === "get-ruler") {
			var response = window.recruitr.getRuler();
			chrome.runtime.sendMessage({ name: "ruler", response: response });

		} else if (message.name === "compose") {
			if (document.readyState !== "complete") {
				document.addEventListener("readystatechange", function(){
					if (document.readyState !== "complete") {
						return;
					}
					window.recruitr.compose(message.rulerInfos, message.message);
				}, false);
			}
			else {
				window.recruitr.compose(message.rulerInfos, message.message);
			}

		} else if (message.name === "alert") {
			alert(message.message);
			console.log(message.message);
		}
	});
}
