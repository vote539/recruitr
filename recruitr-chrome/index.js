// When the extension is installed or upgraded ...
chrome.runtime.onInstalled.addListener(function() {
	// Replace all rules ...
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
		// With a new rule ...
		chrome.declarativeContent.onPageChanged.addRules([
			{
				// That fires when a page's URL contains a 'g' ...
				conditions: [
					new chrome.declarativeContent.PageStateMatcher({
						pageUrl: { hostEquals: "www.cybernations.net" },
					})
				],
				// And shows the extension's page action.
				actions: [ new chrome.declarativeContent.ShowPageAction() ]
			}
		]);
	});
});

function getPasscode(next) {
	chrome.storage.sync.get("prefs", function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.passcode);
	});
}
function getDefaultMessage(next) {
	chrome.storage.sync.get("prefs", function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.defaultMessage);
	});
}
function getFollowUpMessage(next) {
	chrome.storage.sync.get("prefs", function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.followUpMessage);
	});
}
function getRecruitmentMessageSubjects(next) {
	chrome.storage.sync.get("prefs", function(result){
		if (!result.prefs) return next([]);
		return next(result.prefs.messages.map(function(message){ return message.subject }));
	});
}
function getFollowedUpRulers(next) {
	chrome.storage.sync.get("followedUpRulers", function(result){
		if (!result.followedUpRulers) return next([]);
		return next(result.followedUpRulers);
	});
}
function addFollowedUpRulers(newRulers, next) {
	chrome.storage.sync.get("followedUpRulers", function(result){
		rulers = result.followedUpRulers;
		if (!rulers) rulers = [];
		rulers = rulers.concat(newRulers);
		chrome.storage.sync.set({ "followedUpRulers": rulers }, next);
	});
}

function attachContentScripts(next) {
	chrome.tabs.executeScript(null, { file: "content.js" }, function(){
		chrome.tabs.executeScript(null, { file: "content-chrome.js" }, function(){
			next();
		});
	});
}

var currentTab = null;
var handler = null;

// Called when the user clicks on the browser action.
chrome.pageAction.onClicked.addListener(function(tab) {
	currentTab = tab;
	handler = new RecruitrHandler();

	getPasscode(function(passcode){
		if (!handler.testPasscode(passcode)) {
			alert("You must enter the correct password in order to use this extension.");
			return;
		}

		handler.doGetRuler = function() {
			chrome.tabs.sendMessage(currentTab.id, { name: "get-ruler" });
		};
		handler.doOutbox = function() {
			getFollowedUpRulers(function(rulers){
				getRecruitmentMessageSubjects(function(subjects){
					handler.onOutbox(rulers, subjects);
				});
			});
		};
		handler.doGoToCompose = function(url) {
			var composeCallback = function(tabId, changeInfo, tab) {
				if (tabId === currentTab.id && changeInfo.status === "complete") {
					handler.onCompose();
					chrome.tabs.onUpdated.removeListener(composeCallback);
				}
			}
			chrome.tabs.onUpdated.addListener(composeCallback);
			chrome.tabs.update(null, { url: url }, function(){
				// handler.onCompose();
			});
		};
		handler.doCompose = function(rulerInfos, whichMessage) {
			attachContentScripts(function() {
				var getMessageFn = (whichMessage === "default") ? getDefaultMessage : getFollowUpMessage;
				getMessageFn(function(message) {
					chrome.tabs.sendMessage(currentTab.id, {
						name: "compose",
						rulerInfos: rulerInfos,
						message: message
					});
				});
			});
		};
		handler.doAlert = function(message) {
			alert(message);
		};
		handler.doAddFollowedUpRulers = function(rulers) {
			addFollowedUpRulers(rulers, function(){});
		};
		handler.doDestroy = function() { };

		attachContentScripts(function() {
			chrome.tabs.sendMessage(currentTab.id, { name: "get-source" });
		});
	});
});

chrome.runtime.onMessage.addListener(function(message) {
	if (message.name === "source") {
		handler.onSource(message.response);
	}
	else if (message.name === "ruler") {
		handler.onRuler(message.response);
	}
});
