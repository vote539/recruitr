window.savePrefs = function(prefs) {
	browser.storage.sync.set({ "prefs": prefs }).then(function(){
		alert("Your settings have been saved.");
	});
};

window.clearRulers = function(next) {
	browser.storage.sync.set({ "followedUpRulers": [] }).then(function(){
		window.rulersCleared();
	});
};

browser.storage.sync.get(["prefs", "followedUpRulers"]).then(function(results){
	if (!results.prefs) return window.setPrefs(null);
	var rulers = results.followedUpRulers;
	if (!rulers) rulers = [];
	window.setPrefs(results.prefs, rulers.length);
});
