/*
 * Each time a tab is updated, reset the page action for that tab.
 */
browser.tabs.onUpdated.addListener((id, changeInfo, tab) => {
	if (tab.url.match(/^\w+:\/\/www.cybernations.net\//)) {
		browser.pageAction.show(id);
	}
});

function getPasscode(next) {
	browser.storage.sync.get("prefs").then(function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.passcode);
	}).catch(function(){
		return next(null);
	});
}
function getDefaultMessage(next) {
	browser.storage.sync.get("prefs").then(function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.defaultMessage);
	}).catch(function(){
		return next(null);
	});
}
function getFollowUpMessage(next) {
	browser.storage.sync.get("prefs").then(function(result){
		if (!result.prefs) return next(null);
		return next(result.prefs.followUpMessage);
	}).catch(function(){
		return next(null);
	});
}
function getRecruitmentMessageSubjects(next) {
	browser.storage.sync.get("prefs").then(function(result){
		if (!result.prefs) return next([]);
		return next(result.prefs.messages.map(function(message){ return message.subject }));
	}).catch(function(){
		return next(null);
	});
}
function getFollowedUpRulers(next) {
	browser.storage.sync.get("followedUpRulers").then(function(result){
		if (!result.followedUpRulers) return next([]);
		return next(result.followedUpRulers);
	}).catch(function(){
		return next(null);
	});
}
function addFollowedUpRulers(newRulers, next) {
	browser.storage.sync.get("followedUpRulers").then(function(result){
		rulers = result.followedUpRulers;
		if (!rulers) rulers = [];
		rulers = rulers.concat(newRulers);
		browser.storage.sync.set({ "followedUpRulers": rulers }).finally(next);
	}).catch(function(){
		return next(null);
	});
}

function attachContentScripts(next) {
	browser.tabs.executeScript({ file: "/content.js" }).then(function(){
		browser.tabs.executeScript({ file: "/content-quantum.js" }).then(function(){
			next();
		});
	});
}

var currentTab = null;
var handler = null;

// Called when the user clicks on the browser action.
browser.pageAction.onClicked.addListener(function(tab) {
	currentTab = tab;
	handler = new RecruitrHandler();

	getPasscode(function(passcode){
		var authenticated = handler.testPasscode(passcode);
		attachContentScripts(function() {
			if (authenticated) {
				browser.tabs.sendMessage(currentTab.id, { name: "get-source" });
			} else {
				handler.doAlert("You must enter the correct password in order to use this extension.");
			}
		});

		handler.doGetRuler = function() {
			browser.tabs.sendMessage(currentTab.id, { name: "get-ruler" });
		};
		handler.doOutbox = function() {
			getFollowedUpRulers(function(rulers){
				getRecruitmentMessageSubjects(function(subjects){
					handler.onOutbox(rulers, subjects);
				});
			});
		};
		handler.doGoToCompose = function(url) {
			var composeCallback = function(tabId, changeInfo, tab) {
				if (tabId === currentTab.id && changeInfo.status === "complete") {
					handler.onCompose();
					chrome.tabs.onUpdated.removeListener(composeCallback);
				}
			}
			chrome.tabs.onUpdated.addListener(composeCallback);
			chrome.tabs.update(null, { url: url }, function(){
				// handler.onCompose();
			});
		};
		handler.doCompose = function(rulerInfos, whichMessage) {
			attachContentScripts(function() {
				var getMessageFn = (whichMessage === "default") ? getDefaultMessage : getFollowUpMessage;
				getMessageFn(function(message) {
					browser.tabs.sendMessage(currentTab.id, {
						name: "compose",
						rulerInfos: rulerInfos,
						message: message
					});
				});
			});
		};
		handler.doAlert = function(message) {
			browser.tabs.sendMessage(currentTab.id, {
				name: "alert",
				message: message
			});
		};
		handler.doAddFollowedUpRulers = function(rulers) {
			addFollowedUpRulers(rulers, function(){});
		};
		handler.doDestroy = function() { };
	});
});

browser.runtime.onMessage.addListener(function(message) {
	if (message.name === "source") {
		handler.onSource(message.response);
	}
	else if (message.name === "ruler") {
		handler.onRuler(message.response);
	}
});
