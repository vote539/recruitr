var buttons = require("sdk/ui/button/action");
var panels = require("sdk/panel");
var self = require("sdk/self");
var ss = require("sdk/simple-storage");
var tabs = require("sdk/tabs");
var RecruitrHandler = require("./background");

function getPasscode() {
	if (!ss.storage.prefs) return null;
	return ss.storage.prefs.passcode;
}
function getDefaultMessage() {
	if (!ss.storage.prefs) return null;
	return ss.storage.prefs.defaultMessage;
}
function getFollowUpMessage() {
	if (!ss.storage.prefs) return null;
	return ss.storage.prefs.followUpMessage;
}
function getRecruitmentMessageSubjects() {
	if (!ss.storage.prefs) return [];
	return ss.storage.prefs.messages.map(function(message){ return message.subject });
}
function getFollowedUpRulers() {
	if (!ss.storage.followedUpRulers) return [];
	return ss.storage.followedUpRulers;
}
function addFollowedUpRulers(rulers) {
	if (!ss.storage.followedUpRulers) ss.storage.followedUpRulers = [];
	ss.storage.followedUpRulers = ss.storage.followedUpRulers.concat(rulers);
}
function clearFollowedUpRulersCache() {
	ss.storage.followedUpRulers = [];
}
function getPrefs() {
	if (!ss.storage.prefs) return null;
	return ss.storage.prefs;
}
function setPrefs(prefs) {
	ss.storage.prefs = prefs;
}

// Options Panel
var panel = panels.Panel({
	contentURL: self.data.url("options.html"),
	contentScriptFile: [
		self.data.url("knockout-3.4.2.js"),
		self.data.url("options.js"),
		self.data.url("options-firefox.js")
	]
});
panel.port.on("save-prefs", function(prefs) {
	setPrefs(prefs);
	panel.port.emit("alert", "Your preferences have been saved.");
});
panel.port.on("clear-rulers", function() {
	clearFollowedUpRulersCache();
	panel.port.emit("rulers-cleared");
});

// Toolbar Button
var contentScriptInfo = {
	contentScriptFile: [
		self.data.url("content.js"),
		self.data.url("content-firefox.js")
	]
};
var button = buttons.ActionButton({
	id: "recruitr",
	label: "Recruitr by sffc1",
	icon: {
		"16": "./icon-16.png",
		"32": "./icon-32.png",
		"64": "./icon-64.png"
	},
	onClick: function() {
		if (/www.cybernations.net/.test(tabs.activeTab.url)) {
			var tab = tabs.activeTab;
			var worker = tab.attach(contentScriptInfo);
			var handler = new RecruitrHandler();

			if (!handler.testPasscode(getPasscode())) {
				worker.port.emit("alert", "You must enter the correct password in order to use this extension.");
				worker.destroy();
				return;
			}

			worker.port.on("source", handler.onSource);
			worker.port.on("ruler", handler.onRuler);
			worker.port.emit("get-source");

			handler.doGetRuler = function() {
				worker.port.emit("get-ruler");
			};
			handler.doOutbox = function() {
				handler.onOutbox(getFollowedUpRulers(), getRecruitmentMessageSubjects());
			};
			handler.doGoToCompose = function(url) {
				tab.url = url;
				tab.once("ready", handler.onCompose);
			};
			handler.doCompose = function(rulerInfos, whichMessage) {
				worker = tab.attach(contentScriptInfo);
				worker.port.emit("compose", rulerInfos,
					(whichMessage === "default") ? getDefaultMessage() : getFollowUpMessage());
			};
			handler.doAlert = function(message) {
				worker.port.emit("alert", message);
			};
			handler.doAddFollowedUpRulers = function(rulers) {
				addFollowedUpRulers(rulers);
			};
			handler.doDestroy = function() {
				worker.destroy();
			};
		} else {
			panel.show();
			panel.port.emit("set-prefs", getPrefs(), getFollowedUpRulers().length);
		}
	}
});
