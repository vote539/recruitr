window.savePrefs = function(prefs) {
	self.port.emit("save-prefs", prefs);
};

self.port.on("set-prefs", function(prefs, numRulers) {
	window.setPrefs(prefs, numRulers);
});

window.clearRulers = function() {
	self.port.emit("clear-rulers");
};

self.port.on("rulers-cleared", function(prefs, numRulers) {
	window.rulersCleared();
});

self.port.on("alert", function(message) {
	alert(message);
});
