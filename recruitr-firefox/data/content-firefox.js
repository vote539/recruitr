self.port.on("get-source", function() {
	var response = window.recruitr.getSource.apply(this, arguments);
	self.port.emit("source", response);
});

self.port.on("get-ruler", function() {
	var response = window.recruitr.getRuler.apply(this, arguments);
	self.port.emit("ruler", response);
});

self.port.on("compose", function() {
	window.recruitr.compose.apply(this, arguments);
});

self.port.on("alert", function(message) {
	alert(message);
});
